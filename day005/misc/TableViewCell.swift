//
//  TableViewCell.swift
//  day005
//
//  Created by Nguyen Tam Anh Bui on 1/12/18.
//  Copyright © 2018 Nguyen Tam Anh Bui. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    // MARK : Properties
    @IBOutlet weak var nameButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
