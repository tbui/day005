//
//  FirstViewController.swift
//  day005
//
//  Created by Nguyen Tam Anh Bui on 1/12/18.
//  Copyright © 2018 Nguyen Tam Anh Bui. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class FirstViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate{
    
    // MARK : Properties
    
    @IBOutlet weak var mapView: MKMapView!
    var locationManager: CLLocationManager!
    var locations = [Location] ()
    var zoomLocation : Location?
    
    
    // Once this property is set the map looks for the searched items
    // to annotate the map with
    var mapItems: [MKMapItem]? {
        didSet {
            var searchedLocations = [Location] ()
            for item in mapItems! {
                var name: String
                if item.name != nil {
                    name = item.name!
                }
                else {
                    name = ""
                }
                guard let location = Location(title: name, subtitle: name, coordinate: item.placemark.coordinate) else {
                    fatalError("Error in instantiation of location1")
                }
                searchedLocations += [location]
            }
            pinLocations(locationPinArr: searchedLocations)
        }
    }
    
    
    // MARK : methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.showsUserLocation = true
        if (CLLocationManager.locationServicesEnabled())
        {
            print("Activating CLLocationManager")
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        // Loading the example locations
        let tableView = TableViewController()
        tableView.loadLocations()
        let locationPinArr = tableView.locations
        pinLocations(locationPinArr: locationPinArr)
    }
    
    func pinLocations(locationPinArr: [Location]) {
        print("Inside of pinLocations")
        for locationPin in locationPinArr {
            
            //print()
            
            mapView.addAnnotation(locationPin)
        }
        
    }
    
    // Setting the region to the user's local region at first
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if zoomLocation != nil {
            zoomIntoLocationPin()
        }
    }
    
    @IBAction func mapTypeSelection(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            self.mapView.mapType = MKMapType.standard
        case 1:
            self.mapView.mapType = MKMapType.hybrid
        case 2:
            self.mapView.mapType = MKMapType.satellite
        default:
            self.mapView.mapType = MKMapType.standard
        }
    }
    
    // The acting method for the Current Location button
    
    @IBAction func zoomIntoCurrentLocation(_ sender: UIButton) {
        let spanX = 0.00725
        let spanY = 0.00725
        
        let centerCoordinate = CLLocationCoordinate2D(latitude: self.mapView.userLocation.coordinate.latitude, longitude: self.mapView.userLocation.coordinate.longitude)
        
        let centerSpan = MKCoordinateSpanMake(spanX, spanY)
        let region = MKCoordinateRegion(center: centerCoordinate, span: centerSpan)
        self.mapView.setRegion(region, animated: true)
    }
    
    // Zooms into the given location when segue is performed
    
    func zoomIntoLocationPin() {
        let spanX = 0.00725
        let spanY = 0.00725
        let centerSpan = MKCoordinateSpanMake(spanX, spanY)
        
        print("zooming into location in zoomIntoLocationPin")
        guard let location = zoomLocation else {
            fatalError("Location error in FirstViewController")
        }
        let region = MKCoordinateRegion(center: location.coordinate, span: centerSpan)
        self.mapView.setRegion(region, animated: true)
    }
}

