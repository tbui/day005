//
//  AddLocationViewController.swift
//  day005
//
//  Created by Nguyen Tam Anh Bui on 1/24/18.
//  Copyright © 2018 Nguyen Tam Anh Bui. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class AddLocationViewController: UIViewController, CLLocationManagerDelegate {
    
    
    // MARK : Properties
    
    @IBOutlet weak var searchField: UITextField!
    var locationManager: CLLocationManager!
    var centerCoordinate: CLLocationCoordinate2D!
    
    // MARK : private methods
    
    func createSearchRequest(searchedLocation: String) -> MKLocalSearch {
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = searchedLocation
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        request.region = MKCoordinateRegion(center: centerCoordinate, span: span)
        
        let search = MKLocalSearch(request: request)
  
        return search
    }
    // Setting the region to the user's local region at first
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0]
        let long = userLocation.coordinate.longitude;
        let lat = userLocation.coordinate.latitude;
        self.centerCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
    }

    // MARK : View start up functions
    override func viewDidLoad() {
        super.viewDidLoad()
        print("In Add location view")
        if (CLLocationManager.locationServicesEnabled())
        {
            print("Activating CLLocationManager")
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("Preparing segue in AddLocationViewController")
        if (segue.identifier == "searchLocations") {
            let mapvc = segue.destination as? FirstViewController
            
            guard let searched = searchField.text else {
                fatalError("search button error in the AddLocationViewController")
            }
            let search = createSearchRequest(searchedLocation: searched)
            search.start {
                (response: MKLocalSearchResponse!, error: Error?) in
                mapvc?.mapItems = response.mapItems
            }
            
        }
        
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
}
