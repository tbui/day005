//
//  TableViewController.swift
//  day005
//
//  Created by Nguyen Tam Anh Bui on 1/12/18.
//  Copyright © 2018 Nguyen Tam Anh Bui. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class TableViewController: UITableViewController {
    
    var locations = [Location] ()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadLocations()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "LocsTableVielCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                       for: indexPath) as? TableViewCell else {
                                                        fatalError("Downcast failed")
        }
        
        let location = locations[indexPath.row]
        cell.nameButton.setTitle(location.title, for: .normal)
        // loading the tap gesture recognizer into every of the table cells
        return cell
    }
    
    
    // MARK : for the example locations
    
    func loadLocations() {
        
        let examples = ExampleLocations()
        
        // Example locations center coordinates
        let centerCoordinateNotre = CLLocationCoordinate2D(latitude: examples.notreDameLat, longitude: examples.notreDameLong)
        let centerCoordinateSchool = CLLocationCoordinate2D(latitude: examples.schoolLat, longitude: examples.schoolLong)
        let centerCoordinateHouse = CLLocationCoordinate2D(latitude: examples.houseLat, longitude: examples.houseLong)
        guard let location1 = Location(title: "Ecole 42", subtitle: "School", coordinate: centerCoordinateSchool) else {
            fatalError("Error in instantiation of location1")
        }
        guard let location2 = Location(title: "My House", subtitle: "Casa", coordinate: centerCoordinateHouse) else {
            fatalError("Error in instantiation of location2")
        }
        guard let location3 = Location(title: "Notre Dame", subtitle: "Church", coordinate: centerCoordinateNotre) else {
            fatalError("Error in instantiation of location3")
        }
        locations += [location1, location2, location3]
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showMap") {
            let mapvc = segue.destination as? FirstViewController
            guard let senderButton = sender as? UIButton else {
                fatalError("Sender button not present")
            }
            
            // getting the chosen location name through the UIButton
            guard let locationName = senderButton.titleLabel?.text  else {
                fatalError("no location name")
            }
            
            // filtering out the proper location chosen
            let getLocation : [Location]? = locations.filter({ (Location) -> Bool in
                return locationName == Location.title
            })
            
            guard let zoomLocation = getLocation?[0] else {
                fatalError("No valid location was selected in TableViewController")
            }
            mapvc?.zoomLocation = zoomLocation
            
        }
    }
    
    
}
