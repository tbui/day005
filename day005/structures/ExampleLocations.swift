//
//  ExampleLocations.swift
//  day005
//
//  Created by Nguyen Tam Anh Bui on 1/25/18.
//  Copyright © 2018 Nguyen Tam Anh Bui. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

struct ExampleLocations {

    // MARK : Longitudes and latitudes for three example locations.

    let notreDameLat : CLLocationDegrees = 48.852968
    let notreDameLong : CLLocationDegrees = 2.349902
    let schoolLat : CLLocationDegrees = 48.896607
    let schoolLong : CLLocationDegrees = 2.318501
    let houseLat : CLLocationDegrees = 50.092675
    let houseLong : CLLocationDegrees = 8.696277

}
