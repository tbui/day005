//
//  File.swift
//  day005
//
//  Created by Nguyen Tam Anh Bui on 1/12/18.
//  Copyright © 2018 Nguyen Tam Anh Bui. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class Location: NSObject, MKAnnotation {
    var title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D

    
    init ? (title: String, subtitle: String, coordinate: CLLocationCoordinate2D) {
        
        if title.isEmpty || subtitle.isEmpty {
            return nil
        }
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        super.init()
        var subtitle: String? {
            return subtitle
        }
    }
}


